const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3050 ;
const app = express();
const cors = require('cors');
//const multer = require('multer');




// CORS settings

app.use(cors());


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({  
    extended: true
}));

// MySql
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'ecomsa'
});

/*let storage = multer.diskStorage({
  destination:(req, file, cb) => {
    cb(null, './uploads')
  },
  fileName:(req, file, cb) => {
    cb(null, fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});*/

//const upload = multer({ storage});

// Route
app.get('/', (req, res) => {
  res.send('Backend funcionando!!');
});

// método para listar
app.get('/datos', (req, res) => {
  const sql = 'SELECT * FROM t_datos_usuario WHERE estatus = 1';

  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results);
    } else {
      res.send('Sin registros todavía');
    }
  });
});

// método para buscar registro 
app.get('/datos/:id', (req, res) => {
  const { id } = req.params;
  const sql = `SELECT * FROM t_datos_usuario WHERE id = ${id}`;
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result);
    } else {
      res.send('Sin resultados');
    }
  });
});

// Método para insertar registro

app.post('/agregar', (req, res) => {
  const sql = 'INSERT INTO t_datos_usuario SET ?';

  const nuevoDato = {
    nombre: req.body.nombre,
    telefono: req.body.telefono,
    correo: req.body.correo,
    foto: req.body.foto,
    estatus: req.body.estatus
  };

  connection.query(sql, [nuevoDato], error => {
    if (error) throw error;
    console.log('El registro fue insertado correctamente');
    res.send(nuevoDato);
  });
});

// Método para actualizar registro
app.put('/actualizar/:id', (req, res) => {
  const { id } = req.params;
  const { nombre, telefono, correo, foto } = req.body;
  const sql = `UPDATE t_datos_usuario SET nombre = '${nombre}', telefono='${telefono}', correo='${correo}', foto='${foto}' WHERE id =${id}`;

  connection.query(sql, error => {
    if (error) throw error;
    console.log('El registro fue actualizado correctamente');
  });
});

app.put('/desactivar/:id', (req, res) => {
  const { id } = req.params;
  const sql = `UPDATE t_datos_usuario SET estatus = false WHERE id =${id}`;

  connection.query(sql, error => {
    if (error) throw error;
    console.log('El registro fue eliminado correctamente');
  }); 
});

// Método para borrar permanentemente registro (SÓLO UTILIZAR CON AUTORIZACIÓN)

/*app.delete('/eliminar/:id', (req, res) => {
  const { id } = req.params;
  const sql = `DELETE FROM t_datos_usuario WHERE id= ${id}`;

  connection.query(sql, error => {
    if (error) throw error;
    console.log('El registro fue eliminado correctamente');
  });
});*/

// Verificación de conexión
connection.connect(error => {
  if (error) throw error;
  console.log('La base de datos funciona correctamente!');
});

app.listen(PORT, () => console.log(`El servidor está corriendo en el puerto ${PORT}`));
